	db.users.find(
		{
		$or: [
					{"firstName":
						{$regex: "A", $options: "i"}
					},
					{"lastName":
						{$regex: "A", $options: "i"}
					},
			]
		},	
		{
			"_id": 0,
		}
	);

	db.users.find(
		{
			$and: [
					{"isAdmin?": "yes"}, 
					{"isActive?": "yes"}
				]
		}
	);

	db.courses.find(
		{
		"Name": {$regex: "u", $options: "i"},
		"Price": {$gte: "13,000"}
		}
	);